
const UPLOAD_URL = "../tempimg/";
const progress = document.querySelector("progress");
var sending = false;

function killEvent(e)
{
	e.preventDefault();
	e.stopPropagation();
}

document.body.addEventListener("dragover", killEvent); // Otherwise it just opens the file anyway

document.body.ondrop = e => {
	killEvent(e);

	if(sending)
		return;

	const { files } = e.dataTransfer;
	if(files.length !== 1)
		return window.alert("Vous ne devez envoyer qu'un fichier.");

	const [file] = files;
	if(!file.type.startsWith("image/"))
		return window.alert("Vous devez envoyer une image.");

	if(file.size > 5 * 10**7) // 50 Mo
		return window.alert("Cette image est trop grosse. (max : 50 Mo)");
	if(file.size === 0)
		return window.alert("Cette image est vide.");

	progress.max = file.size;
	progress.value = 0;
	progress.hidden = false;

	const xhr = new XMLHttpRequest(); // not fetch because it doesn't allow seeing the progress
	xhr.open("POST", UPLOAD_URL);
	xhr.setRequestHeader("Content-Type", file.type);

	xhr.upload.addEventListener("progress", e => progress.value = e.loaded);

	xhr.onreadystatechange = function() {
		if(this.readyState !== XMLHttpRequest.DONE)
			return;

		if(this.status === 201)
			document.body.style.backgroundImage = `url("${this.getResponseHeader("Location")}")`;
		else
			window.alert(`Error (status code ${this.status})\n${this.responseText}`);

		progress.hidden = true;
		sending = false;
	}

	xhr.send(file);
}
