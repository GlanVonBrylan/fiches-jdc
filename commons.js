
const changeMainBackground = setBackground.bind(document.body, "Indiquez l'URL de l'image de fond");

export const actions = {
	f: changeMainBackground,
	p: saveCard,
};

export let actionsDisabled = false;


document.body.onkeydown = async event => {
	if(!event.ctrlKey || actionsDisabled)
		return;

	const action = actions[event.key.toLowerCase()];
	if(action)
	{
		event.preventDefault();
		action();
	}
}


export function setActionsDisabled(disabled)
{
	actionsDisabled = disabled;
	for(const button of document.querySelectorAll("aside button"))
		button.disabled = disabled;
}


async function setBackground(prompt)
{
	const url = window.prompt(prompt || "Indiquez l'URL de l'image");
	if(url !== null)
	{
		try {
			const response = await fetch(url, { method: "HEAD" });
			if(response.ok)
				this.style.backgroundImage = `url("${url}")`;
			else
				window.alert(`L'image n'a pas pu être récupérée (${response.statusText})`);
		} catch (e) {
			this.style.backgroundImage = `url("../proxy?url=${encodeURIComponent(url)}")`;
		}
	}
}

function switchImg()
{
	const { dataset } = this;
	dataset.current ??= 0;
	let newAlt = ++dataset.current;
	if(!dataset[`alt${newAlt}`])
		dataset.current = newAlt = 0;
	this.src = dataset[`alt${newAlt}`];
	this.title = dataset[`title${newAlt}`] || dataset.titleDefault || "";
}

function toggleImg()
{
	const not = this.querySelector(".not");
	not.hidden = !not.hidden;
}



for(const elm of document.querySelectorAll("img[switch]"))
{
	const parent = elm.parentElement;
	parent.title = "Cliquez pour inverser";
	parent.onclick = switchImg.bind(elm);
}

for(const elm of document.querySelectorAll("img[toggle]"))
{
	const not = Object.assign(document.createElement("img"), {
		src: "/icones/not.svg",
		className: "not",
		hidden: true,
	});
	elm.parentElement.appendChild(not);
	elm.parentElement.onclick = toggleImg;
}

for(const img of document.getElementsByTagName("img"))
{
	const { dataset } = img;
	if(img.hasAttribute("switch"))
		dataset.alt0 = img.src;
	const alts = img.alts = [];
	for(let i = 0 ; `alt${i}` in dataset ; i++)
		alts.push(`alt${i}`);

	if(dataset.title0)
		dataset.titleDefault = img.title;
}


import "./html2canvas.min.js";

document.getElementById("save").onclick = saveCard;

export async function saveCard(fileName)
{
	if(!fileName || typeof fileName === "object")
		fileName = location.pathname.replaceAll("/", "");
	
	setActionsDisabled(true);
	saveFile(`${fileName}.jpg`, await getPicture());
	setActionsDisabled(false);
}

export async function getPicture(asBlob)
{
	// https://html2canvas.hertzen.com/configuration
	const canvas = await html2canvas(document.body, {
		useCORS: true,
		ignoreElements: elm => elm.hasAttribute("no-print"),
		windowWidth: 1920, windowHeight: 1080,
	});
	return asBlob
		? toBlob(canvas, "image/jpeg", 0.95)
		: canvas.toDataURL("image/jpeg", 0.95).replace("image/jpeg", "image/octet-stream");
}

function toBlob(canvas, ...args) {
	return new Promise(res => canvas.toBlob(res, ...args));
}

export function saveFile(fileName, dataURL)
{
	const link = document.createElement("a");
	link.download = fileName;
	link.href = dataURL;
	link.click();
}