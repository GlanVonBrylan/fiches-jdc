
import { avatarRoot, onGladiatorChange } from "/gladiateurs.js";
import { actions } from "/commons.js";

const icons = document.querySelectorAll("img[src*=\".svg\"]");

const getById = document.getElementById.bind(document);

const earlyAccess = getById("earlyAccess");
actions["e"] = () => earlyAccess.hidden = !earlyAccess.hidden;

onGladiatorChange(({ name, background, iconBg, text }) => {
	document.body.style.color = text || iconBg;
	getById("bandeau").style.backgroundImage = `url("${avatarRoot}/${name}/Bandeau.png")`;
	getById("face").style.backgroundImage = `url("${avatarRoot}/${name}/Logo.png")`;

	const colors = `?color=${background.replace("#", "%23")}&sColor=${iconBg.replace("#", "%23")}`;
	for(const icon of icons)
	{
		const { src, dataset } = icon;
		if(src.includes("color="))
		{
			icon.src = src.substring(0, src.indexOf("?"));
			for(const alt of icon.alts)
				dataset[alt] = dataset[alt].substring(0, dataset[alt].indexOf("?"));
		}

		icon.src += colors;
		for(const alt of icon.alts)
			dataset[alt] += colors;
	}

	for(const elm of document.querySelectorAll("[icon]"))
		elm.style.backgroundColor = iconBg;

	getById("price").style.color = background;
});
