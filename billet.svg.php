<?php require 'svg.php'; ?><!-- Generator: Adobe Illustrator 25.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" id="Calque_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 134 80" style="enable-background:new 0 0 134 80;" xml:space="preserve">
<style type="text/css">
	.st0{fill:<?=COLOR?>;}
	.st1{fill:<?=COLOR?>;}
</style>
<path class="st0" d="M108.5,10.2c2,8.3,6.9,13.6,15.4,15.7c0,9.3,0,18.6,0,28.1c-3.6,1.2-7.3,2.7-10.1,5.7
	c-2.7,2.8-4.2,6.3-5.3,10.2c-27.6,0-55,0-82.9,0c-2-7.9-6.8-13.7-15.5-15.6c0-9.4,0-18.6,0-28.2c8-2.1,13.5-7.2,15.3-15.8
	C53,10.2,80.5,10.2,108.5,10.2z"/>
<rect x="-0.3" y="-8" class="st1" width="4.4" height="93.1"/>
<rect x="130.5" y="-1.2" class="st1" width="4.4" height="86.1"/>
<rect x="64.7" y="11.5" transform="matrix(-1.879285e-07 1 -1 -1.879285e-07 144.6607 10.8949)" class="st1" width="4.4" height="132.5"/>
<rect x="64.7" y="-64.2" transform="matrix(-1.686452e-07 1 -1 -1.686452e-07 68.9432 -64.8227)" class="st1" width="4.4" height="132.5"/>
</svg>
